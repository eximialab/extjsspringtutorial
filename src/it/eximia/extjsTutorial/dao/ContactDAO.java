package it.eximia.extjsTutorial.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import it.eximia.extjsTutorial.model.Contact;

/**
 * Contact DAO class.
 * 
 * @author Eximia devTeam
 * http://www.eximia.it
 *  
 */
@Repository
public class ContactDAO implements IContactDAO{
	
	private HibernateTemplate hibernateTemplate;

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		hibernateTemplate = new HibernateTemplate(sessionFactory);
	}
	
	/**
	 * Get List of contacts from database
	 * @return list of all contacts
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Contact> getContacts() {
                // carico tutti i contatti
		return hibernateTemplate.find("from Contact");
	}

	/**
	 * Delete a contact with the id passed as parameter
	 * @param id
	 */
	@Override
	public void deleteContact(int id){
                // carico il contatto
		Object record = hibernateTemplate.load(Contact.class, id);
                // lo cancello
                hibernateTemplate.delete(record);
	}
	
	/**
	 * Create a new Contact on the database or
	 * Update contact
	 * @param contact
	 * @return contact added or updated in DB
	 */
	@Override
	public Contact saveContact(Contact contact){
                // salvo o aggiorno
		hibernateTemplate.saveOrUpdate(contact);
		return contact;
	}

}
