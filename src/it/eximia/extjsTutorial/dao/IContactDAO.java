package it.eximia.extjsTutorial.dao;

import java.util.List;

import it.eximia.extjsTutorial.model.Contact;

public interface IContactDAO {

	List<Contact> getContacts();
	
	void deleteContact(int id);
	
	Contact saveContact(Contact contact);
	
}
