package it.eximia.extjsTutorial.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import it.eximia.extjsTutorial.model.Contact;
import it.eximia.extjsTutorial.service.ContactService;

/**
 * Controller - Spring
 * 
 * @author Eximia devTeam
 * http://www.eximia.it
 * 
 * E' il controller del bo, si occupa di mappare richieste fatte da fe e computare i dati relativi
 * Nel caso il frontend come in questo caso sia erogato da un'app html5, non serve implementare alcuno strato di view
 * ma unicamente lo strato di controller e di restituzione dei dati
 
si noti che si possono definire pi� controllers, spring si occupa di fare lo scanning di tutti i controllers e
a meno di conflitti li attiva tutti
occhio per�, tipicamente i controller vanno definiti in maniera furba e non invasiva, tipicamente app semplici come 
il trasf non richiedono pi� controllers, semplicemente di definire in maniera ordinata i mapping esattamente
come faremmo organizzando in alberatura le nostre jsp
**/

@Controller
public class ContactController  {

        // � il servizio che si occupa di fornire al core i metodi e gli accessori per accedere alle entities 'contact'
	private ContactService contactService;
	
        /*
        Commento i primi due metodi, gli altri sono tutti identici
        La direttiva @RequestMapping permette di mappare il metodo implementato a seguire in una chiamata web
        Alias, se definisco un requestMapping /contact/view.action, la chiamata web a /extjs-spring-tutorial/contact/view.action viene risolta 
        con il metodo implementato appena sotto
        */
	@RequestMapping(value="/contact/view.action")
        /*
        Questo � il metodo implementato dalla RequestMapping
        Le direttive @ResponseBody e @RequestBody servono a serializzare/deserializzare request e response in modo che siano
        compatibili con il formato atteso sia in termini di dati in arrivo che di dati prodotti dal frontend
        il 'trick' qui sta nel fatto che la reponseBody sia composta da una mappa generica di stringhe e relativi oggetti generici
        */
	public @ResponseBody Map<String,? extends Object> view() throws Exception {

		try{

                        // chiedo a contactService, che chiede a contactDAO, la lista di tutti i contatti
			List<Contact> contacts = contactService.getContactList();
                        
                        // restituisco una mappa di chhiave/valore compatibile con le specifiche extJs, 
                        // guardare il metodo in fondo al controller
			return getMap(contacts);

		} catch (Exception e) {

			return getModelMapError("Error retrieving Contacts from database.");
		}
	}
	
        /*
        Ora guardiamo il metodo create, che oltre ad una response si vede inviata la lista dei dati per creare un 
        nuovo contact
        si noti la annotation @RequestParam di tipo object
        */
	@RequestMapping(value="/contact/create.action")
	public @ResponseBody Map<String,? extends Object> create(@RequestParam Object data) throws Exception {

		try{
                        // dato l'oggetto 'data', richiamo il metodo create di contactService che a sua volta richiama il metodo create di contactDAO
                        // andiamo a vedere il metodo creare di contactService, che si occupa di 'tradurre' data in una lista di oggetti 'contacts'
			List<Contact> contacts = contactService.create(data);

			return getMap(contacts);

		} catch (Exception e) {

			return getModelMapError("Error trying to create contact.");
		}
	}
	
	@RequestMapping(value="/contact/update.action")
	public @ResponseBody Map<String,? extends Object> update(@RequestParam Object data) throws Exception {
		try{

			List<Contact> contacts = contactService.update(data);

			return getMap(contacts);

		} catch (Exception e) {

			return getModelMapError("Error trying to update contact.");
		}
	}
	
	@RequestMapping(value="/contact/delete.action")
	public @ResponseBody Map<String,? extends Object> delete(@RequestParam Object data) throws Exception {
		
		try{

			contactService.delete(data);

			Map<String,Object> modelMap = new HashMap<String,Object>(3);
			modelMap.put("success", true);

			return modelMap;

		} catch (Exception e) {

			return getModelMapError("Error trying to delete contact.");
		}
	}
	
	/**
	 * Generates modelMap to return in the modelAndView
	 * @param contacts
	 * @return
	 */
	private Map<String,Object> getMap(List<Contact> contacts){
		// il metodo restituisce, come da specifiche extJs una mappa composta da 3 elementi
                // total, data e success
                // si noti come contacts non debba essere in alcun modo wrappato, trattato o cos'altro perch� 
                // ci pensa la direttiva @ResponseBody del metodo chiamante
            
		Map<String,Object> modelMap = new HashMap<String,Object>(3);
		modelMap.put("total", contacts.size());
		modelMap.put("data", contacts);
		modelMap.put("success", true);
		
		return modelMap;
	}
	
	/**
	 * Generates modelMap to return in the modelAndView in case
	 * of exception
	 * @param msg message
	 * @return
	 */
	private Map<String,Object> getModelMapError(String msg){

		Map<String,Object> modelMap = new HashMap<String,Object>(2);
		modelMap.put("message", msg);
		modelMap.put("success", false);

		return modelMap;
	} 

        /*
        AutoWire di contactService
        */
	@Autowired
	public void setContactService(ContactService contactService) {
		this.contactService = contactService;
	}

}
