package it.eximia.extjsTutorial.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.eximia.extjsTutorial.dao.ContactDAO;
import it.eximia.extjsTutorial.model.Contact;
import it.eximia.extjsTutorial.util.Util;

/**
 * Contact Service
 * 
 * @author Eximia devTeam
 * http://www.eximia.it
 *  
 */
@Service
public class ContactService {
	
	private ContactDAO contactDAO;
	private Util util;

	/**
	 * Get all contacts
	 * @return
	 */
	@Transactional(readOnly=true)
	public List<Contact> getContactList(){

		return contactDAO.getContacts();
	}
	
	/**
	 * Create new Contact/Contacts
	 * @param data - json data from request
	 * @return created contacts
	 */
	@Transactional
        /*
        Qui il metodo che prende i dati json inviati e li trasforma (attraverso un metodo di util, in lista di contatti
        */
	public List<Contact> create(Object data){
		
        List<Contact> newContacts = new ArrayList<Contact>();
		
                // trasformo il json inviato in una lista (1 o >1 in funzione dei dati inviati) di contatti
		List<Contact> list = util.getContactsFromRequest(data);
		
		for (Contact contact : list){
                        // attraverso il dao semplicissimo salvo il contatto
			newContacts.add(contactDAO.saveContact(contact));
		}
		
		return newContacts;
	}
	
	
	/**
	 * Update contact/contacts
	 * @param data - json data from request
	 * @return updated contacts
	 */
	@Transactional
	public List<Contact> update(Object data){
		
		List<Contact> returnContacts = new ArrayList<Contact>();
		// costruisco i contatti dal json inviato
		List<Contact> updatedContacts = util.getContactsFromRequest(data);
		
		for (Contact contact : updatedContacts){
                        // salvo i contatti usando il DAO
			returnContacts.add(contactDAO.saveContact(contact));
		}
		
		return returnContacts;
	}
	
	/**
	 * Delete contact/contacts
	 * @param data - json data from request
	 */
	@Transactional
	public void delete(Object data){
		
		//it is an array - have to cast to array object
		if (data.toString().indexOf('[') > -1){
			
			List<Integer> deleteContacts = util.getListIdFromJSON(data);
			
			for (Integer id : deleteContacts){
				contactDAO.deleteContact(id);
			}
			
		} else { //it is only one object - cast to object/bean
			
			Integer id = Integer.parseInt(data.toString());
			
			contactDAO.deleteContact(id);
		}
	}
	

	/**
	 * Spring use - DI
	 * @param contactDAO
	 */
	@Autowired
	public void setContactDAO(ContactDAO contactDAO) {
		this.contactDAO = contactDAO;
	}

	/**
	 * Spring use - DI
	 * @param util
	 */
	@Autowired
	public void setUtil(Util util) {
		this.util = util;
	}
	
}
